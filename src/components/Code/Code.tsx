import React, { useState, useRef, createRef, KeyboardEvent, ClipboardEvent, FocusEvent } from 'react';

import { Button } from 'antd';
import { fetchGraphql } from '../../api/Graphql';

interface Props {
  setStep: React.Dispatch<React.SetStateAction<'code' | 'signup' | 'success'>>;
}

const GET_INVITATION_CODE = `
  query getInvitationCode($code: String){
    exists: getInvitationCode(code: $code)
  }
`;
interface GetInvitationCode {
  exists: boolean;
}

export function Code({ setStep }: Props) {
  const [splittedCode, setSplittedCode] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>();

  const numberOfInputs = [1, 2, 3, 4, 5, 6];
  const inputsRef = useRef(numberOfInputs.map(() => createRef() as React.MutableRefObject<HTMLInputElement>));

  const handleChange = (options: { event: React.ChangeEvent<HTMLInputElement>; currentIndex: number }) => {
    const value = options.event.target.value;
    const currentIndex = options.currentIndex;
    const newCode = splittedCode;
    splittedCode[currentIndex] = value;
    setSplittedCode(newCode);
  };

  const handlePaste = (options: { event: ClipboardEvent<HTMLInputElement> }) => {
    const clipboardData = options.event.clipboardData.getData('Text');
    const splittedClipboardData = clipboardData.split('');
    inputsRef.current.forEach((input, index) => {
      input.current.value = splittedClipboardData[index];
      input.current.blur();
    });
    const newCode = splittedClipboardData.slice(0, numberOfInputs.length);
    setSplittedCode(newCode);
  };

  const handleKeyDown = (options: { event: KeyboardEvent<HTMLInputElement>; currentIndex: number }) => {
    const event = options.event;
    const currentIndex = options.currentIndex;
    const value = event.currentTarget.value;
    const nextInput = inputsRef.current[currentIndex + 1]?.current;
    const prevInput = inputsRef.current[currentIndex - 1]?.current;

    if (event.keyCode === 8) {
      if (!prevInput) return;
      prevInput.focus();
      return;
    }
    if (!value) return;
    if (!nextInput) return;
    nextInput.focus();
  };

  const handleFocus = (options: { event: FocusEvent<HTMLInputElement> }) => {
    return options.event.target.select();
  };

  const handleVerifyCode = async () => {
    setIsLoading(true);
    const code = splittedCode.join('');

    if (!code) {
      setError('No tienes un codigo');
      setIsLoading(false);
      return;
    }

    const { exists } = await fetchGraphql<GetInvitationCode>({
      query: GET_INVITATION_CODE,
      variables: { code },
    });

    if (exists) {
      setStep('signup');
    } else {
      setError('Tu código es incorrecto.');
    }
    setIsLoading(false);
  };

  return (
    <>
      <div className='input-numbers'>
        <div className='input'>
          {numberOfInputs.map((_input, currentIndex) => (
            <input
              type='text'
              placeholder=' '
              maxLength={1}
              onChange={event => handleChange({ event, currentIndex })}
              onPaste={event => handlePaste({ event })}
              onKeyUp={event => handleKeyDown({ event, currentIndex })}
              onFocus={event => handleFocus({ event })}
              ref={inputsRef.current[currentIndex]}
              key={currentIndex}
            />
          ))}
        </div>

        {error && (
          <div className='error'>
            <p>{error}</p>
          </div>
        )}
        <Button className='priv-btn' shape='round' size='large' onClick={handleVerifyCode} loading={isLoading}>
          Continuar
        </Button>
      </div>
      <p>El acceso a Priv es exclusivo por invitacion.</p>
      <p>Si aun no tienes invitacion, puedes pedirsela a laguien que ya tenga acceso o escribir a hola@mipriv.com</p>
    </>
  );
}
