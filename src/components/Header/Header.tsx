import React from 'react';

import '../../styles/Header.scss';

export function Header() {
  return (
    <header>
      <div className='big-container'>
        <img alt='Ícono de Priv' src='/logo.png' className='logo' />
      </div>
    </header>
  );
}
