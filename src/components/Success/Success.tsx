import React from 'react';

interface Props {
  username: string;
}

export function Success({ username }: Props) {
  return (
    <div>
      <h1>Bienvenido {username}!</h1>
      <p>Gracias por formar parte de MiPriv</p>
      <p>
        <a href={`https://www.mipriv.com/${username}`} target='_blank' rel='noopener noreferrer'>
          Ir a MiPriv
        </a>
      </p>
    </div>
  );
}
