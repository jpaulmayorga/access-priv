import React, { useState } from 'react';

import { Form, Input, Button } from 'antd';
import { useFirebaseAuth } from '../../hooks/useFirebaseAuth';

import slugify from 'slugify';
import TextArea from 'antd/lib/input/TextArea';
import { fetchGraphql } from '../../api/Graphql';

interface Props {
  setIsSignup: React.Dispatch<React.SetStateAction<boolean>>;
  setUsername: React.Dispatch<React.SetStateAction<string>>;
}

const MUTATION_USER = `
  mutation MutationUser($input: CreateUserInput) {
    user: createUser(input: $input) {
      _id
      auth_id
      created_at
      is_creator
      name
      username
      email
      avatar
      about
    }
  }
`;

interface GetUser {
  user: {
    _id: string;
    auth_id: string;
    created_at: string;
    is_creator: boolean;
    name: string;
    username: string;
    email: string;
    avatar: string;
    about: string;
  };
}

const QUERY_IS_AVAILABLE = `
  query isAvailable($username: String){
    isAvailable: getIsUsernameAvailable(username: $username)
  }
`;
interface getIsAvailable {
  isAvailable: boolean;
}

export function Signup({ setIsSignup, setUsername }: Props) {
  const { firebaseAuth } = useFirebaseAuth();
  const [form] = Form.useForm();

  const [isLoading, setIsLoading] = useState<boolean>();
  const [error, setError] = useState<string>();

  const makeLogin = async (data: any) => {
    const { email, password, username, name, price, about, paypal_account } = data;
    const formattedUsername = slugify(username, {
      replacement: '-',
      lower: true,
      strict: true,
    });
    setUsername(formattedUsername);
    try {
      setIsLoading(true);
      const { isAvailable } = await fetchGraphql<getIsAvailable>({
        query: QUERY_IS_AVAILABLE,
        variables: { username: formattedUsername },
      });
      if (!isAvailable) {
        throw new Error('El nombre de usuario esta tomado.');
      }

      const { user } = await firebaseAuth.createUserWithEmailAndPassword(email, password);
      if (user) {
        await fetchGraphql<GetUser>({
          query: MUTATION_USER,
          variables: {
            input: {
              auth_id: user.uid,
              email,
              is_creator: true,
              name,
              price: Number(price),
              about,
              username: formattedUsername,
              paypal_account,
              max_followers_per_day: 10,
            },
          },
        });
      }
      setIsSignup(true);
    } catch (error) {
      console.log('logging error:', error);
      setError(error.message);
      setIsLoading(false);
    }
  };

  return (
    <div>
      <Form form={form} className='login-box' onFinish={makeLogin}>
        <div className='header'>
          <h1>Hola!</h1>
          <p>Registrate para ser un creador</p>
        </div>
        <Form.Item name='name' rules={[{ required: true, message: 'Error: Falta tu nombre.' }]}>
          <Input placeholder='Nombre' size='large' />
        </Form.Item>
        <Form.Item name='email' rules={[{ required: true, type: 'email', message: 'Error: Tu email no es válido.' }]}>
          <Input placeholder='Correo electronico' size='large' />
        </Form.Item>
        <Form.Item
          name='password'
          rules={[
            { required: true, message: 'Error: Tu contraseña no es válida.' },
            { min: 6, message: 'Tu contraseña debe tener minimo 6 caracteres' },
          ]}>
          <Input type='password' placeholder='Contraseña' size='large' />
        </Form.Item>
        <Form.Item name='username' rules={[{ required: true, message: 'Error: Tu username no es válido.' }]}>
          <Input placeholder='Nombre de usuario' size='large' />
        </Form.Item>
        <Form.Item
          name='price'
          rules={[
            { required: true, message: 'Error: Falta tu precio.' },
            { min: 1, message: 'El precio debe ser minimo 1 peso' },
          ]}>
          <Input placeholder='Precio' size='large' type='number' />
        </Form.Item>
        <Form.Item
          name='paypal_account'
          rules={[{ required: true, type: 'email', message: 'Error: Tu Paypal no es válido.' }]}>
          <Input placeholder='Cuenta de Paypal' size='large' />
        </Form.Item>
        <Form.Item name='about' rules={[{ required: true, message: 'Error: Falta completar algo acerca de ti.' }]}>
          <TextArea placeholder='Acerca de ti'></TextArea>
        </Form.Item>
        {error && (
          <div className='error'>
            <p>{error}</p>
          </div>
        )}
        <Form.Item>
          <Button className='priv-btn' htmlType='submit' shape='round' size='large' block loading={isLoading}>
            Crear cuenta mi cuenta
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
