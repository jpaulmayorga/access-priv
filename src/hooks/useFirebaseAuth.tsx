import React, { createContext, ReactNode, useEffect, useContext, useState } from 'react';
import firebaseNs from 'firebase/app';

interface FirebaseContext {
  isAuthenticated: boolean;
  isLoadingAuth: boolean;
  firebase: typeof firebaseNs;
}
const FirebaseContext = createContext<FirebaseContext | null>(null);

interface FirebaseProvider {
  firebase: typeof firebaseNs;
  children: ReactNode;
}

export function FirebaseAuthProvider({ firebase, children }: FirebaseProvider) {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [isLoadingAuth, setIsLoadingAuth] = useState<boolean>(false);

  useEffect(() => {
    setIsLoadingAuth(true);
    firebase.auth().onAuthStateChanged(async (user: any) => {
      if (user) {
        setIsAuthenticated(true);
      } else {
        setIsAuthenticated(false);
      }
      setIsLoadingAuth(false);
    });
  }, [firebase]);

  return (
    <FirebaseContext.Provider value={{ isAuthenticated, isLoadingAuth, firebase }}>{children}</FirebaseContext.Provider>
  );
}

export function useFirebaseAuth() {
  const firebaseContext = useContext(FirebaseContext);

  if (firebaseContext === null) {
    throw new Error('No FirebaseAuthProvider found.');
  }

  const { isAuthenticated, isLoadingAuth, firebase } = firebaseContext;
  const firebaseAuth = firebase.auth();

  return {
    isAuthenticated,
    isLoadingAuth,
    firebaseAuth,
  };
}
