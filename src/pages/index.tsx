import React, { useState, useEffect } from 'react';

import { Header } from '../components/Header/Header';
import { Code } from '../components/Code/Code';

import '../styles/Access.scss';
import { Signup } from '../components/Signup/Signup';
import { Success } from '../components/Success/Success';

export function App() {
  const [isSignup, setIsSignup] = useState<boolean>(false);
  const [username, setUsername] = useState<string>('');
  const [step, setStep] = useState<'code' | 'signup' | 'success'>('code');

  useEffect(() => {
    if (isSignup && username) {
      setStep('success');
    }
  }, [isSignup, username]);

  return (
    <>
      <Header />
      <div className='access'>
        <div className='container'>
          <img src='/Secure@2x.png' alt='Lock' className='icon' />
          <h1>Bienvenido a Priv!</h1>

          <p className='white'>Introduce tu codigo de invitacion.</p>
          {step === 'code' && <Code setStep={setStep} />}
          {step === 'signup' && <Signup setIsSignup={setIsSignup} setUsername={setUsername} />}
          {step === 'success' && <Success username={username} />}
        </div>
      </div>
    </>
  );
}
