import axios from 'axios';

const GRAPHQL_URL = 'https://api.mipriv.com/graphql';

export async function fetchGraphql<Data>(options: { query: string; variables?: any }): Promise<Data> {
  const request = await axios.post(
    GRAPHQL_URL,
    {
      query: options.query,
      variables: options.variables,
    },
    {
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
  return await request.data.data;
}
