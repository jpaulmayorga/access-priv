import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './pages';

import 'antd/dist/antd.css';
import './styles/global.scss';

import { FirebaseAuthProvider } from './hooks/useFirebaseAuth';
import firebase from 'firebase/app';
import 'firebase/auth';
import { X_FIREBASE_CONFIG } from './config';

// we check if firebase is not initialized yet
if (!firebase.apps.length) {
  firebase.initializeApp(X_FIREBASE_CONFIG);
}

ReactDOM.render(
  <React.StrictMode>
    <FirebaseAuthProvider firebase={firebase}>
      <App />
    </FirebaseAuthProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
