export const DEFAULT_AVATAR =
  'https://res.cloudinary.com/dmjdqri5b/image/upload/v1592347884/dev/266cb001832fcaa01c784d96a320d6d1_erd8zk.jpg';
export const DEFAULT_COVER = 'https://res.cloudinary.com/dmjdqri5b/image/upload/v1593201536/Banner_rwio5x.jpg';

export const X_AIRTABLE_API_KEY = process.env.REACT_APP_AIRTABLE_API_KEY || '';
export const X_AIRTABLE_BASE_ID = process.env.REACT_APP_AIRTABLE_BASE_ID || '';
export const X_CLOUDINARY_NAME = process.env.REACT_APP_CLOUDINARY_NAME || '';
export const X_CLOUDNIARY_PRESSET = process.env.REACT_APP_LOUDINARY_PRESET || '';

export const X_FIREBASE_CONFIG = {
  apiKey: 'AIzaSyCI0ZmuN75tdNnH-egF5TCaBb81NAxdLoQ',
  authDomain: 'mi-priv.firebaseapp.com',
  databaseURL: 'https://mi-priv.firebaseio.com',
  projectId: 'mi-priv',
  storageBucket: 'mi-priv.appspot.com',
  messagingSenderId: '937052427306',
  appId: '1:937052427306:web:15665e440c7142566a9f53',
  measurementId: 'G-EDMV3HQPPV',
};
